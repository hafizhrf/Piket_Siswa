<?php
    if(!isset($_GET['action'])){
        echo "<script>url:location='../View/user/list.php';</script>"; 
    }
    include ("../Function/user.php");
    if($_GET['action'] == "tambahadmin"){
        $id = $_POST["id"];
        $uname = $_POST["uname"];
        $pass = $_POST["pass"];
        $add = tambah_admin($id,$uname,$pass);
        if($add){
            header("location:../View/user/list.php");
        }	
        else{
            echo "<script>alert('Useername/ID Sudah Ada');</script>";
            echo "<script>url:location='../View/user/tambahadmin.php';</script>";
        }
    }
    elseif($_GET['action'] == "tambah"){
        $nis = $_POST["NIS"];
        $id = $nis;
        $uname = $_POST["uname"];
        $pass = $_POST["pass"];
            $tambah = tambah_user($nis,$id,$uname,$pass);
                if($tambah){
                    header("location:../View/user/list.php");
                }	
                else{
                    echo "<script>alert('Username/ID Sudah Ada');</script>";
                    echo "<script>url:location='../View/user/tambah.php';</script>";
                }
    }
    elseif($_GET['action'] == "delete"){
        $id = $_GET["id"];
        $hapus = hapus_user($id);
        if($hapus){
            header("location:../View/user/list.php");
        }	
        else{
            echo "<script>alert('Delete Failed');</script>";
        }
    }
    elseif($_GET['action'] == "edit"){
        $id = $_GET['id'];
        $uname = $_POST["uname"];
        $pass = $_POST["pass"];
            $edit = update_user($id,$uname,$pass);
                if($edit){
                    header("location:../View/user/list.php");
                }	
                else{
                    echo "<script>alert('Username Sudah Ada');</script>";
                    echo "<script>url:location='../View/user/edit.php?id=$id';</script>";
                }
    }
    else{
        echo "<script>url:location='../View/user/list.php';</script>"; 
    }
?>