<?php
    if(!isset($_GET['action'])){
        echo "<script>url:location='../View/jurusan/jurusan.php';</script>"; 
    }
    include ("../Function/jurusan.php");
    if($_GET['action'] == "update"){
        $id = $_GET["id"];
        $jurusan = $_POST["jrsn"];
        $update = update_jurusan($id,$jurusan);
        if($update){
            header("location:../View/jurusan/jurusan.php");
        }	
        else{
            echo "<script>alert('Edit Failed');</script>";
        }
    }
    elseif($_GET['action'] == "tambah"){
        $id = $_POST["id"];
        $jurusan = $_POST["jurusan"];
        if($jurusan == "" | $id == ""){
            echo "<script>alert('isi dulu dong');</script>";
            echo "<script>url:location='../View/jurusan/tambah.php';</script>";
        }
        elseif($jurusan == " " | $id == " "){
            echo "<script>alert('isi dulu dong');</script>";
            echo "<script>url:location='../View/jurusan/tambah.php';</script>";
        }
        else{
            $tambah = tambah_jurusan($id,$jurusan);
                if($tambah){
                    header("location:../View/jurusan/jurusan.php");
                }	
                else{
                    echo "<script>alert('Add Failed');</script>";
                }
        }
    }
    elseif($_GET['action'] == "delete"){
        $id = $_GET["id"];
        $hapus = hapus_jurusan($id);
        if($hapus){
            header("location:../View/jurusan/jurusan.php");
        }	
        else{
            echo "<script>alert('Delete Failed');</script>";
        }
    }
    else{
        echo "<script>url:location='../View/jurusan/jurusan.php';</script>"; 
    }
?>