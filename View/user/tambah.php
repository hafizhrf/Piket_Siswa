<?php
    include ('../../Function/user.php');
?>
<html>
    <head>
        <title>Tambah User</title>
    </head>
    <body>
        <div>
            <table>
                <form action="../../Process/user.php?action=tambah" method="POST">
                    <tr>
                        <td>Nama Siswa</td>
                        <td><select name="NIS">
                        <?php
                            $nis = read_nis();
                            foreach ($nis as $value) 
                            {
                        ?>
                            <option value="<?php echo $value['nis']; ?>"><?= $value['nama']; }?>
				        </select>
                    </td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" name="uname"></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="text" name="pass"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="tambah"></td>
                        <td></td>
                    </tr>
                </form>
            </table>
        </div>
    </body>
</html>