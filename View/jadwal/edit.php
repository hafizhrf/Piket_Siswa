<?php
    if(!isset($_GET['id'])){
        echo "<script>url:location='list.php';</script>"; 
    }
    include ('../../Function/jadwal.php');
    $edit = edit_jadwal($_GET['id']);
    foreach($edit as $value){
?>
<html>
    <head>
        <title>Edit Jadwal Piket</title>
    </head>
    <body>
        <div>
            <table>
                <form action="../../Process/jadwal.php?action=update&id=<?= $value['id'] ?>" method="POST">
                    <tr>
                        <td>Nama Kelompok</td>
                        <td><select name="kel">
                        <?php
                            $kelo = read_kelompok();
                            foreach ($kelo as $kel) 
                            {
                        ?>
                            <option value="<?php echo $kel['id']; ?>"><?= $kel['nama_kelompok']; }?>
				        </select>
                    </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td><input type="date" name="tgl" value="<?= $value['tanggal'] ?>"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="edit" value="edit"></td>
                        <td></td>
                    </tr>
                    <?php
                    }
                    ?>
                </form>
            </table>
        </div>
    </body>
</html>