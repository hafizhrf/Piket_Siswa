<?php
    include ('../../Function/jadwal.php');
?>
<html>
    <head>
        <title>Tambah Jadwal Piket</title>
    </head>
    <body>
        <div>
            <table>
                <form action="../../Process/jadwal.php?action=tambah" method="POST">
                    <tr>
                        <td>ID</td>
                        <td><input type="text" name="id"></td>
                    </tr>
                    <tr>
                        <td>Nama Kelompok</td>
                        <td><select name="kel">
                        <?php
                            $kel = read_kelompok();
                            foreach ($kel as $value) 
                            {
                        ?>
                            <option value="<?php echo $value['id']; ?>"><?= $value['nama_kelompok']; }?>
				        </select>
                    </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td><input type="date" name="tgl"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="tambah"></td>
                        <td></td>
                    </tr>
                </form>
            </table>
        </div>
    </body>
</html>